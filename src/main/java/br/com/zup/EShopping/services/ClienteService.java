package br.com.zup.EShopping.services;

import br.com.zup.EShopping.dtos.ClienteDto;
import br.com.zup.EShopping.exceptions.DuplicatesException;
import br.com.zup.EShopping.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService {
    private List<ClienteDto> clientes = new ArrayList<>();

    public ClienteService() {
    }

    public List<ClienteDto> getClientes() {
        return clientes;
    }

    public void setClientes(List<ClienteDto> clientes) {
        this.clientes = clientes;
    }

    public ClienteDto cadastrarCliente(ClienteDto novoCliente) {
        clientes.add(novoCliente);

        return novoCliente;
    }

    public ClienteDto pesquisarCliente(String cpf) {
        for (ClienteDto elementoLista : clientes) {
            if (elementoLista.getCpf().equals(cpf)) {
                return elementoLista;
            }
        }

        throw new NotFoundException("Cliente não encontrado");
    }

    public void verificarClienteDuplicadoPorCpf(String cpf) {
        for (ClienteDto elementoLista : clientes) {
            if (elementoLista.getCpf().equals(cpf)) {
                throw new DuplicatesException("Cliente já existe");
            }
        }
    }

    public void verificarClienteDuplicadoPorEmail(String email) {
        for (ClienteDto elementoLista : clientes) {
            if (elementoLista.getEmail().equals(email)) {
                throw new DuplicatesException("Já existe um cliente com esse e-mail");
            }
        }
    }

}

