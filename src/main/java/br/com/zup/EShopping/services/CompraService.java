package br.com.zup.EShopping.services;

import br.com.zup.EShopping.dtos.ClienteDto;
import br.com.zup.EShopping.dtos.CompraDto;
import br.com.zup.EShopping.dtos.ProdutoDto;
import br.com.zup.EShopping.exceptions.ProdutoEsgotadoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CompraService {
    private List<CompraDto> compras = new ArrayList<>();

    @Autowired
    private ProdutoService produtoService;
    @Autowired
    private ClienteService clienteService;

    public CompraService() {
    }

    public List<CompraDto> getCompras() {
        return compras;
    }

    public void setCompras(List<CompraDto> compras) {
        this.compras = compras;
    }

    public List<CompraDto> mostrarTodasCompras() {
        return compras;
    }

    public CompraDto cadastrarCompra(CompraDto novaCompra) {
        ClienteDto clienteDaCompra = clienteService.pesquisarCliente(novaCompra.getCliente().getCpf());
        novaCompra.setCliente(clienteDaCompra);

        List<ProdutoDto> listaDeCompras = new ArrayList<>();

        for (ProdutoDto elementoLista : novaCompra.getProdutos()) {
            ProdutoDto produto = produtoService.pesquisarProduto(elementoLista.getNome());
            listaDeCompras.add(produto);
        }
        produtoService.verificaProdutosEsgotados(listaDeCompras);
        novaCompra.setProdutos(listaDeCompras);

        compras.add(novaCompra);

        return novaCompra;
    }

    public List<ProdutoDto> mostrarComprasDeUmCliente(String cpf) {
        List<ProdutoDto> produtosComprados = new ArrayList<>();

        for (CompraDto elementoLista : compras) {
            if (elementoLista.getCliente().getCpf().equals(cpf)) {
                for (ProdutoDto elementoListaProduto : elementoLista.getProdutos()) {
                    produtosComprados.add(elementoListaProduto);
                }
            }
        }
        return produtosComprados;
    }

}

