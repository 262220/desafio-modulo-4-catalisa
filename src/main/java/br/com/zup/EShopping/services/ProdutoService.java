package br.com.zup.EShopping.services;

import br.com.zup.EShopping.dtos.ProdutoDto;
import br.com.zup.EShopping.exceptions.DuplicatesException;
import br.com.zup.EShopping.exceptions.NotFoundException;
import br.com.zup.EShopping.exceptions.ProdutoEsgotadoException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProdutoService {
    private List<ProdutoDto> produtos = new ArrayList<>();

    public ProdutoService() {
    }

    public List<ProdutoDto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<ProdutoDto> produtos) {
        this.produtos = produtos;
    }

    public ProdutoDto cadastrarProduto(ProdutoDto novoProduto) {
        this.produtos.add(novoProduto);

        return novoProduto;
    }

    public void verificarProdutoDuplicado(String nome) {
        for (ProdutoDto elementoLista : produtos) {
            if (elementoLista.getNome().equals(nome)) {
                throw new DuplicatesException("Esse produto já foi cadastrado");
            }
        }
    }

    public List<ProdutoDto> mostrarProdutosCadastrados() {
        return produtos;
    }

    public ProdutoDto pesquisarProduto(String nome) {
        for (ProdutoDto elementoLista : produtos) {
            if (elementoLista.getNome().equals(nome)) {
                return elementoLista;
            }
        }

        throw new NotFoundException("Produto não encontrado");
    }

    public void verificaProdutosEsgotados(List<ProdutoDto> produtos) {
        List<ProdutoDto> produtosForaDeEstoque = new ArrayList<>();
        for (ProdutoDto elementoLista : produtos) {
            if (elementoLista.getQuantidade() == 0) {
                produtosForaDeEstoque.add(elementoLista);
            }
        }

        if (produtosForaDeEstoque != null) {
            throw new ProdutoEsgotadoException("Você não pode comprar produtos esgotados, segue os produtos esgotados em seu carrinho:", produtosForaDeEstoque);
        }
    }

}

