package br.com.zup.EShopping.dtos;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ProdutoDto {
    @Size(min = 2, message = "{validacao.produto.nome}")
    private String nome;
    @DecimalMin(value = "0.1", message = "{validacao.produto.preco}")
    private double preco;
    @Min(value = 0, message = "{validacao.produto.quantidade}")
    private int quantidade;

    public ProdutoDto() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}

