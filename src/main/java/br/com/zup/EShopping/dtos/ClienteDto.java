package br.com.zup.EShopping.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

public class ClienteDto {
    @Size(min = 2, message = "{validacao.cliente.nome}")
    private String nome;
    @Size(min = 11, max = 11, message = "{validacao.cliente.cpf}")
    private String cpf;
    @Email(message = "{validacao.cliente.email}")
    private String email;

    public ClienteDto() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

