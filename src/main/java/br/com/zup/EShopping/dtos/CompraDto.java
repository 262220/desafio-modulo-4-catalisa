package br.com.zup.EShopping.dtos;

import br.com.zup.EShopping.dtos.ClienteDto;
import br.com.zup.EShopping.dtos.ProdutoDto;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

public class CompraDto {
    @Valid
    private ClienteDto cliente;
    @Valid
    private List<ProdutoDto> produtos = new ArrayList<>();

    public CompraDto() {
    }

    public ClienteDto getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDto cliente) {
        this.cliente = cliente;
    }

    public List<ProdutoDto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<ProdutoDto> produtos) {
        this.produtos = produtos;
    }
}

