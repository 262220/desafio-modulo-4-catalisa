package br.com.zup.EShopping.controllers;

import br.com.zup.EShopping.dtos.ClienteDto;
import br.com.zup.EShopping.dtos.CompraDto;
import br.com.zup.EShopping.dtos.ProdutoDto;
import br.com.zup.EShopping.services.CompraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/compras")
public class CompraController {
    @Autowired
    private CompraService compraService;

    @PostMapping
    public CompraDto adicionarCompra(@RequestBody CompraDto novaCompra) {
        return compraService.cadastrarCompra(novaCompra);
    }

    @GetMapping
    public List<CompraDto> mostrarCompras() {
        return compraService.mostrarTodasCompras();
    }

    @GetMapping("/obtercompras/{cpf}")
    public List<ProdutoDto> mostrarComprasDeUmCliente(@PathVariable String cpf) {
        return compraService.mostrarComprasDeUmCliente(cpf);
    }
}
