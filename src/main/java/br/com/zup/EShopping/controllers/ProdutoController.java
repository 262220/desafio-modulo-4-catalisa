package br.com.zup.EShopping.controllers;

import br.com.zup.EShopping.dtos.ProdutoDto;
import br.com.zup.EShopping.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {
    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    public ProdutoDto adicionarProduto(@RequestBody @Valid ProdutoDto novoProduto) {
        produtoService.verificarProdutoDuplicado(novoProduto.getNome());
        produtoService.cadastrarProduto(novoProduto);

        return novoProduto;
    }

    @GetMapping
    public List<ProdutoDto> mostrarProdutosCadastrados() {
        return produtoService.mostrarProdutosCadastrados();
    }
}

