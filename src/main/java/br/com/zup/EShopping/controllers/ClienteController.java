package br.com.zup.EShopping.controllers;

import br.com.zup.EShopping.dtos.ClienteDto;
import br.com.zup.EShopping.dtos.ProdutoDto;
import br.com.zup.EShopping.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ClienteDto adicionarCliente(@RequestBody @Valid ClienteDto novoCliente) {
        clienteService.verificarClienteDuplicadoPorCpf(novoCliente.getCpf());
        clienteService.verificarClienteDuplicadoPorEmail(novoCliente.getEmail());

        return clienteService.cadastrarCliente(novoCliente);
    }

    @GetMapping("/{cpf}")
    public ClienteDto retornarClientePesquisado(@PathVariable String cpf) {
        return clienteService.pesquisarCliente(cpf);
    }

}

