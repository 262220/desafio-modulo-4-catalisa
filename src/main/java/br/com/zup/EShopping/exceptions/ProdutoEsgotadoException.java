package br.com.zup.EShopping.exceptions;

import br.com.zup.EShopping.dtos.ProdutoDto;

import java.util.ArrayList;
import java.util.List;

public class ProdutoEsgotadoException extends RuntimeException {
    private List<ProdutoDto> produtosEsgotados = new ArrayList<>();

    public ProdutoEsgotadoException(String message, List<ProdutoDto> produtosEsgotados) {
        super(message);
        this.produtosEsgotados = produtosEsgotados;
    }

    public List<ProdutoDto> getProdutosEsgotados() {
        return produtosEsgotados;
    }

    public void setProdutosEsgotados(List<ProdutoDto> produtosEsgotados) {
        this.produtosEsgotados = produtosEsgotados;
    }
}

