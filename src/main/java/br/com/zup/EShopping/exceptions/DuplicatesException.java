package br.com.zup.EShopping.exceptions;

public class DuplicatesException extends RuntimeException {

    public DuplicatesException(String message) {
        super(message);
    }
}

