package br.com.zup.EShopping.exceptions;

import java.util.List;

public class MensagemDeErroCustomizado {
    private int statusCode;
    private List<ErroCustomizado> erros;

    public MensagemDeErroCustomizado(int statusCode, List<ErroCustomizado> erros) {
        this.statusCode = statusCode;
        this.erros = erros;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<ErroCustomizado> getErros() {
        return erros;
    }

    public void setErros(List<ErroCustomizado> erros) {
        this.erros = erros;
    }
}

