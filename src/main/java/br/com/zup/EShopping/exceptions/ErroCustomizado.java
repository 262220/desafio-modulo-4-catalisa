package br.com.zup.EShopping.exceptions;

public class ErroCustomizado {
    private String mensagem;

    public ErroCustomizado(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}

