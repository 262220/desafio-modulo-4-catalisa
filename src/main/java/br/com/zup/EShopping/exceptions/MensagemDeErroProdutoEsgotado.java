package br.com.zup.EShopping.exceptions;

import br.com.zup.EShopping.dtos.ProdutoDto;

import java.util.ArrayList;
import java.util.List;

public class MensagemDeErroProdutoEsgotado {
    private int statusCode;
    private List<ErroCustomizado> erros;
    private List<ProdutoDto> produtosEsgotados;

    public MensagemDeErroProdutoEsgotado(int statusCode, List<ErroCustomizado> erros, List<ProdutoDto> produtosEsgotados) {
        this.statusCode = statusCode;
        this.erros = erros;
        this.produtosEsgotados = produtosEsgotados;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<ErroCustomizado> getErros() {
        return erros;
    }

    public void setErros(List<ErroCustomizado> erros) {
        this.erros = erros;
    }

    public List<ProdutoDto> getProdutosEsgotados() {
        return produtosEsgotados;
    }

    public void setProdutosEsgotados(List<ProdutoDto> produtosEsgotados) {
        this.produtosEsgotados = produtosEsgotados;
    }
}

