package br.com.zup.EShopping.exceptions;

import br.com.zup.EShopping.services.CompraService;
import br.com.zup.EShopping.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControllerAdivisor {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro manipularExcecaoDeValidacao(MethodArgumentNotValidException excecao) {
        List<FieldError> fieldErrors = excecao.getBindingResult().getFieldErrors();
        List<Erro> erros = fieldErrors.stream().map(objetoErro -> new Erro(objetoErro.getField(), objetoErro.getDefaultMessage()))
                .collect(Collectors.toList());

        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public MensagemDeErroCustomizado manipularNotFoundExceptions(NotFoundException exception) {
        List<ErroCustomizado> erros = Arrays.asList(new ErroCustomizado(exception.getMessage()));

        return new MensagemDeErroCustomizado(422, erros);
    }

    @ExceptionHandler(DuplicatesException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public MensagemDeErroCustomizado manipularDuplicatesExceptions(DuplicatesException exception) {
        List<ErroCustomizado> erros = Arrays.asList(new ErroCustomizado(exception.getMessage()));

        return new MensagemDeErroCustomizado(422, erros);
    }

    @ExceptionHandler(ProdutoEsgotadoException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public MensagemDeErroProdutoEsgotado manipularProdutoEsgotadoException(ProdutoEsgotadoException exception) {
        List<ErroCustomizado> erros = Arrays.asList(new ErroCustomizado(exception.getMessage()));

        return new MensagemDeErroProdutoEsgotado(422, erros, exception.getProdutosEsgotados());
    }
}

